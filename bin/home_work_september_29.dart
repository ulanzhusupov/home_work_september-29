import 'dart:math';

void main(List<String> arguments) {
  task1();
  // print(task2());
  // print(task3());
  // task4();
//   print(task5());
}

/* Task 1
Напишите программу, которая находит и выводит на экран
наибольший элемент в данном массиве чисел, а также его индекс
*/
void task1() {
  List<int> arr = [1, 2, 3, 4, 5, 6, 7];
  int bigNum = arr[0];
  int index = 0;
  for (int i = 1; i < arr.length; i++) {
    if (arr[i] > bigNum) {
      bigNum = arr[i];
      index = i;
    }
  }

  print("Наибольшее число в массиве: $bigNum");
  print("Индекс наибольшего числа в массиве: $index");
}

/* Task 2
Напишите программу, которая проверяет, все ли элементы в массиве
уникальны (не повторяются). если да, то необходимо вывести true*/
bool task2() {
  List<int> arr = [1, 2, 3, 4, 5, 6, 7];
  for (int i = 0; i < arr.length; i++) {
    for (int j = i + 1; j < arr.length; j++) {
      if (arr[i] == arr[j]) {
        return false;
      }
    }
  }

  return true;
}

/*Task 3
Решите задачу о подсчете количества четных чисел в массиве.
*/
int task3() {
  List<int> arr = [1, 2, 3, 4, 5, 6, 7];
  int countEven = 0;
  for (int i = 0; i < arr.length; i++) {
    if (arr[i] % 2 == 0) countEven++;
  }

  return countEven;
}


/*Task 4
Отсортируйте массив чисел по возрастанию, используя цикл for и условный оператор if-else.
*/

void task4() {
  // List<int> arr = List.generate(20, (index) => Random().nextInt(100));
  List<int> arr = [85, 13, 22, 26, 49, 40, 10, 28, 72, 28, 18, 13, 57, 2, 63, 59, 17, 42, 25, 68];

  arr.sort((a, b) => a-b);
  print(arr);
}


/* Task 5
  Напишите программу, которая находит наибольшее четное число в массиве.
*/

int task5() {
  List<int> arr = [85, 13, 22, 26, 49, 40, 10, 28, 72, 28, 18, 13, 57, 2, 63, 59, 17, 42, 25, 68];

  int bigEven = 0;

  for(int i = 0; i < arr.length; i++) {
    if(arr[i] % 2 == 0 && arr[i] > bigEven) bigEven = arr[i];
  }

  return bigEven;
}